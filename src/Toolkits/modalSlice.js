import { createSlice } from "@reduxjs/toolkit"

const initialState = {
    isOpen: false,
    url: null,
}

const modalSlice = createSlice({
    name: "modalSlice",
    initialState,
    reducers: {
        handleOpenModal: (state, action) => {
            state.isOpen = true;
            state.url = action.payload
        },
        handleCloseModal: (state, action) => {
            state.isOpen = false;
            state.url = null;
        }
    },
})

export const { handleOpenModal, handleCloseModal } = modalSlice.actions

export default modalSlice.reducer