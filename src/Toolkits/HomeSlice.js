import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    isHomePage1 : true , 
}

const HomeSlice = createSlice({
    name: "HomeSlice",
    initialState,
    reducers: {
        setHomePage : (state , action) => {
            state.isHomePage1 = false ; 
        } , 
        setHomePageTrue : (state , action) => {
            state.isHomePage1 = true ; 
        }
    }
});

export const {setHomePage , setHomePageTrue} = HomeSlice.actions

export default HomeSlice.reducer