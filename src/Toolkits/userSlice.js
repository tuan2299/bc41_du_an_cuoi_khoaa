import { createSlice } from '@reduxjs/toolkit'
import { localUserServ } from '../Services/localService';

const initialState = {
    userInfo: localUserServ.get()
}

const userSlice = createSlice({
    name: "userInfo",
    initialState,
    reducers: {
        setUserInfo: (state, action) => { state.userInfo = action.payload }
    }
});

export const { setUserInfo } = userSlice.actions

export default userSlice.reducer