import { UserOutlined, FileTextOutlined, FileSearchOutlined, ProjectOutlined } from '@ant-design/icons';
import { Layout, Menu, theme } from 'antd';
import { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';
import AdminHeader from '../Components/AdminHeader/AdminHeader';
import { localUserServ } from '../Services/localService';
const { Header, Content, Footer, Sider } = Layout;
function getItem(label, key, icon, children) {
  return {
    key,
    icon,
    children,
    label,
  };
}

const items = [
  getItem(<NavLink to='/admin-users'>Users</NavLink>, '1', <UserOutlined />),
  getItem(<NavLink to='/admin-jobs'>Jobs</NavLink>, '2', <FileTextOutlined />),
  getItem(<NavLink to='/admin-types-of-job'>Types of Job</NavLink>, '3', <FileSearchOutlined />),
  getItem(<NavLink to='/admin-services'>Services</NavLink>, '4', <ProjectOutlined />),
];
const AdminLayout = ({ Component }) => {
  const [collapsed, setCollapsed] = useState(false);
  useEffect(() => {
    // kiểm tra user đã đăng nhập và có role admin ko
    let user = localUserServ.get();
    if (!user || user.user.role !== "ADMIN") {
      window.location.href = '/login';
    }
  }, [])
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  return (
    <Layout
      style={{
        minHeight: '100vh',
        // minWidth: '800px',
      }}
    >
      <Sider collapsible collapsed={collapsed} onCollapse={(value) => setCollapsed(value)} breakpoint="lg">
        <Menu theme="dark" defaultSelectedKeys={'1'} mode="inline" items={items} />
      </Sider>
      <Layout className="site-layout">
        <AdminHeader
          style={{
            padding: 0,
            background: colorBgContainer,

          }}>

        </AdminHeader>
        <Content
          style={{
            margin: '16px',
          }}
        >
          <div
            style={{
              minHeight: 360,
            }}
          >
            <Component />
          </div>
        </Content>
      </Layout>
    </Layout>
  );
};
export default AdminLayout;