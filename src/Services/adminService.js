import axios from "axios"
import { configHeader, https } from "./config"

export const adminServ = {
    // api for user
    getUserList: () => {
        return https.get(`/api/users`)
    },
    deleteUser: (userId) => {
        return https.delete(`api/users?id=${userId}`)
    },
    updateUser: (userId, data) => {
        return https.put(`/api/users/${userId}`, data)
    },
    addUser: (userData) => {
        return https.post(`/api/users`, userData)
    },
    getSearchUser: (name) => {
        return https.get(`/api/users/search/${name}`)
    },

    // api for jobs
    getJobsList: () => {
        return https.get(`/api/cong-viec`)
    },
    deleteJob: (jobId) => {
        return https.delete(`/api/cong-viec/${jobId}`)
    },
    updateJob: (jobId, data) => {
        return https.put(`/api/cong-viec/${jobId}`, data)
    },
    addJob: (jobData) => {
        return https.post(`/api/cong-viec`, jobData)
    },

    // api for types of job
    getTypesOfJobList: () => {
        return https.get(`/api/loai-cong-viec`)
    },
    deleteTypesOfJob: (jobId) => {
        return https.delete(`/api/loai-cong-viec/${jobId}`)
    },
    updateTypesOfJob: (jobId, data) => {
        return https.put(`/api/loai-cong-viec/${jobId}`, data)
    },
    addTypesOfJob: (jobData) => {
        return https.post(`/api/loai-cong-viec`, jobData)
    },

    // api for services
    getServicesList: () => {
        return https.get(`/api/thue-cong-viec`)
    },
    deleteService: (jobId) => {
        return https.delete(`/api/thue-cong-viec/${jobId}`)
    },
    updateService: (jobId, data) => {
        return https.put(`/api/thue-cong-viec/${jobId}`, data)
    },
    addService: (jobData) => {
        return https.post(`/api/thue-cong-viec`, jobData)
    },
}
