import { https } from "./config";

export const userSer = {
    postSingup: (acc) => https.post("/api/auth/signup", acc),
    postSingin: (acc) => https.post("/api/auth/signin", acc),
    putUser: (id, value) => https.put(`/api/users/${id}`, value)
}