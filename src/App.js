import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Spinner from './Components/Spinner/Spinner';
import { userRoute } from './routes/userRoute';
import Layout from './Layouts/Layout';
import './css/header.css' ; 

function App() {
  return (
    <BrowserRouter>
      <Spinner />
      <Routes>
        {userRoute.map(({ path, component }, index) => {
          return <Route key={index} path={path} element={component} />
        })}
      </Routes>
    </BrowserRouter>
  );
}

export default App;
