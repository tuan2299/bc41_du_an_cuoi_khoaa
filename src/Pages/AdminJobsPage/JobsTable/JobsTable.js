import React, { useEffect, useState } from 'react'
import { adminServ } from '../../../Services/adminService';
import { useDispatch } from 'react-redux';
import { setLoadingOff, setLoadingOn } from '../../../Toolkits/spinnerSlice';
import { Button, Table, Tooltip, message, Modal } from 'antd';
import { headerColumns } from '../utils';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import '../../../css/userTable.css'
import { showEditModal } from '../../../Toolkits/adminSlice';

export default function JobsTable() {
    const [jobsList, setJobsList] = useState([])
    let dispatch = useDispatch();
    useEffect(() => {
        fetchJobsList()
    }, [])
    let fetchJobsList = () => {
        dispatch(setLoadingOn());
        adminServ.getJobsList()
            .then((res) => {
                dispatch(setLoadingOff());
                let list = res.data.content.map((job) => {
                    return {
                        ...job, action: (
                            <div className='space-x-2 flex items-center '>
                                <Tooltip title="Edit">
                                    <Button onClick={() => { handleEditJob(job) }} shape="circle" icon={<EditOutlined />} className='edit flex items-center justify-center' />
                                </Tooltip>
                                <Tooltip title="Delete">
                                    <Button onClick={() => { showConfirm(job.id) }} shape="circle" icon={<DeleteOutlined />} className='delete flex items-center justify-center' />
                                </Tooltip>
                            </div>
                        )
                    }
                })
                setJobsList(list);
            })
            .catch((err) => {
                dispatch(setLoadingOff());
                console.log(err);
            });
    }
    let handleDeleteJob = (jobId) => {
        dispatch(setLoadingOn())
        adminServ.deleteJob(jobId)
            .then((res) => {
                dispatch(setLoadingOff());
                message.success("Xóa công việc thành công");
                fetchJobsList();
            })
            .catch((err) => {
                dispatch(setLoadingOff());
                message.error("Đã có lỗi xảy ra");
                console.log(err);
            });
    }
    let handleEditJob = (job) => {
        dispatch(showEditModal(job));
    }

    const { confirm } = Modal;
    const showConfirm = (jobId) => {
        confirm({
            icon: '',
            content: (
                <div className='space-y-3'>
                    <h1>Xác nhận xóa công việc này</h1>
                </div>
            ),
            onOk() {
                handleDeleteJob(jobId);
            },
            onCancel() { }
        });
    };

    return (
        <div id='adminTable' className='p-3 bg-white mt-3'>
            <Table dataSource={jobsList} columns={headerColumns} />
        </div>
    );
};
