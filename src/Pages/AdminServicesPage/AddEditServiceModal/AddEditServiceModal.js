import React, { useState } from 'react'
import { Button, Modal, Form, Input, message, Radio, Checkbox } from 'antd';
import '../../../css/addEditModal.css'
import { useDispatch, useSelector } from 'react-redux';
import { handleCloseModal, showAddModal } from '../../../Toolkits/adminSlice';
import { adminServ } from '../../../Services/adminService';

export default function ServicesModal() {
    let dispatch = useDispatch();
    let { isOpenM, serviceInfo } = useSelector(state => state.adminSlice)

    const handleAddService = () => {
        dispatch(showAddModal());
    };
    const handleCancel = () => {
        dispatch(handleCloseModal());
        window.location.reload();
    };

    const onFinish = (values) => {
        if (serviceInfo) {
            adminServ.updateService(values.id, values)
                .then((res) => {
                    message.success('Cập nhật dịch vụ thành công');
                    setTimeout(() => {
                        window.location.reload();
                    }, 1000);
                    window.location.reload();
                })
                .catch((err) => {
                    message.error("Đã có lỗi xảy ra");
                });
        }
        else {
            adminServ.addService(values)
                .then((res) => {
                    message.success('Thêm dịch vụ thành công');
                    setTimeout(() => {
                        window.location.reload();
                    }, 1000);
                })
                .catch((err) => {
                    message.error("Đã có lỗi xảy ra");
                });
        }

    };
    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <div id='addEditServiceModal' className='addEditModal'>
            <Button onClick={handleAddService} className='add'>
                Thêm dịch vụ
            </Button>
            <Modal title={serviceInfo ? "Chỉnh sửa thông tin dịch vụ" : "Thêm dịch vụ mới"} open={isOpenM} footer={false} onCancel={handleCancel}>
                <Form
                    name="basic"
                    labelCol={{
                        span: 10,
                    }}
                    wrapperCol={{
                        span: 24,
                    }}
                    style={{
                        maxWidth: 600,
                    }}
                    initialValues={{
                        id: serviceInfo ? serviceInfo.id : null,
                        maCongViec: serviceInfo ? serviceInfo.maCongViec : null,
                        maNguoiThue: serviceInfo ? serviceInfo.maNguoiThue : null,
                        ngayThue: serviceInfo ? serviceInfo.ngayThue : null,
                        // hoanThanh: serviceInfo ? serviceInfo.hoanThanh : null,
                    }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    autoComplete="off"
                >
                    <Form.Item
                        label="ID"
                        name="id"
                        rules={[
                            {
                                required: true,
                                message: 'Mục không được để trống!',
                            },
                        ]}
                    // className='userID'
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="Mã công việc"
                        name="maCongViec"
                        rules={[
                            {
                                required: true,
                                message: 'Mục không được để trống!',
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="Mã người thuê"
                        name="maNguoiThue"
                        rules={[
                            {
                                required: true,
                                message: 'Mục không được để trống!',
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="Ngày thuê"
                        name="ngayThue"
                        rules={[
                            {
                                required: true,
                                message: 'Mục không được để trống!',
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="Trạng thái hoàn thành"
                        name="hoanThanh"
                        rules={[
                            {
                                required: false,
                            },
                        ]}
                    >
                        <Checkbox />
                    </Form.Item>

                    <Form.Item
                        wrapperCol={{
                            offset: 8,
                            span: 16,
                        }}
                    >
                        <Button htmlType="submit" className='addEditBtn mr-3'>
                            {serviceInfo ? "Cập nhật" : "Thêm"}
                        </Button>
                        <Button onClick={handleCancel}>Hủy</Button>
                    </Form.Item>
                </Form>
            </Modal>
        </div>
    )
}
