import React from 'react'
import { Desktop, LargeDesktop, Mobile, SmallMobile, Tablet } from '../../../Layouts/Responsive'
import ServicesDesktopToAbove from './ServicesDesktopToAbove'
import ServicesTablet from './ServicesTablet'
import ServicesMobile from './ServicesMobile'
import '../../../css/services.css'

export default function Services() {
    return (
        <>
            <SmallMobile>
                <ServicesMobile />
            </SmallMobile>
            <Mobile>
                <ServicesTablet />
            </Mobile>
            <Tablet>
                <ServicesTablet />
            </Tablet>
            <Desktop>
                <ServicesDesktopToAbove />
            </Desktop>
            <LargeDesktop>
                <ServicesDesktopToAbove />
            </LargeDesktop>
        </>
    )
}
