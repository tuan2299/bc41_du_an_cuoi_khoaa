import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { setHomePageTrue } from '../../Toolkits/HomeSlice';
import Banner from './Banner/Banner';
import Services from './PopularServices/Services';
import Benefit from './Benefit/Benefit';
import Testimonial from './Testimonial/Testimonial'
import Marketplace from './Marketplace/Marketplace';

export default function HomePage() {
    let { isHomePage1 } = useSelector((state) => state.HomeSlice);
    let dispatch = useDispatch();
    console.log(isHomePage1);
    useEffect(() => {
        dispatch(setHomePageTrue());
        const handleScroll = () => {
            const scrollY = window.scrollY;
            const currentWidth = window.innerWidth;
            let navbarElement = document.getElementById('navbar__header');
            let categoryElement = document.getElementById('category');
            let searchElement = document.getElementById('search__service');
            // * Đối với màn hình tablet , desktop 
            if (currentWidth >= 768) {
                if (isHomePage1 && scrollY > 0) {
                    navbarElement.classList.remove('header__active');
                    categoryElement.classList.remove('categoriesMenu-active');
                } else {
                    navbarElement.classList.add('header__active');
                    categoryElement.classList.add('categoriesMenu-active');
                }
                // * Đối với màn hình điện thoại     
            } else {
                if (isHomePage1 && scrollY > 0) {
                    navbarElement.classList.remove('header__active');
                    searchElement.classList.add('search__service-active');
                } else {
                    navbarElement.classList.add('header__active');
                    searchElement.classList.remove('search__service-active');
                }
            }
        };
        window.addEventListener('scroll', handleScroll);
        return () => {
            window.removeEventListener('scroll', handleScroll);
        }
    }, [isHomePage1]);
    return (
        <>
            <Banner />
            <Services />
            <Benefit />
            <Testimonial />
            <Marketplace />
        </>
    )
}
