import React from 'react'
import TypesOfJobTable from './TypesOfJobTable/TypesOfJobTable'
import AddEditTypeJobModal from './AddEditTypeJobModal/AddEditTypeJobModal'

export default function AdminTypesOfJobPage() {
    return (
        <div>
            <AddEditTypeJobModal />
            <TypesOfJobTable />
        </div>
    )
}
