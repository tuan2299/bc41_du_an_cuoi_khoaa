import React from 'react'
import { Avatar, Card } from 'antd';
import { HeartFilled, StarFilled } from '@ant-design/icons';
import '../../../css/itemJob.css'
import { NavLink } from 'react-router-dom';
const { Meta } = Card;

export default function ItemJob({ data }) {
    let { hinhAnh, tenCongViec, danhGia, saoCongViec, giaTien } = data.congViec
    let { tenNguoiTao, avatar, id } = data;
    return (
        <div className='col-lg-3 col-md-4 col-sm-6 col-xs-12 mb-4'>
            <Card
                cover={
                    <img
                        alt="example"
                        src={hinhAnh}
                    />
                }
                actions={[
                    <div className='flex justify-between items-center'>
                        <HeartFilled />
                        <p>STARTING AT <span>US${giaTien}</span></p>
                    </div>
                ]}
            >
                <div className='cardTitle flex justify-start items-center'>
                    <Meta avatar={<Avatar src={avatar} />}>
                    </Meta>
                    <div className="name">
                        <h4>{tenNguoiTao}</h4>
                        <p>Level {saoCongViec} Seller</p>
                    </div>
                </div>
                <NavLink to={`/jobDetail/${id}`} className='jobDesc'>{tenCongViec}</NavLink>
                <div className="rating flex justify-start items-center space-x-1 mb-3">
                    <StarFilled />
                    <p>{saoCongViec}</p>
                    <p>({danhGia})</p>
                </div>
            </Card>
        </div>
    )
}
