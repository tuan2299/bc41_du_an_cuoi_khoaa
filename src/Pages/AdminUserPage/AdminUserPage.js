import React from 'react'
import UserTable from './UserTable/UserTable'
import AddEditUserModal from './AddEditUserModal/AddEditUserModal'
import { useSelector } from 'react-redux'
import { useEffect } from 'react'
import { useMemo } from 'react'
import { useCallback } from 'react'

export default function AdminUserPage() {
    return (
        <div>
            <AddEditUserModal />
            <UserTable />
        </div>
    )
}
