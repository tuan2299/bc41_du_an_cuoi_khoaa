import React, { useState } from 'react'
import { Button, Modal, Form, Input, message, Radio } from 'antd';
import '../../../css/addEditModal.css'
import { useDispatch, useSelector } from 'react-redux';
import { handleCloseModal, showAddModal } from '../../../Toolkits/adminSlice';
import { adminServ } from '../../../Services/adminService';

export default function AddEditUserModal() {
    let dispatch = useDispatch();
    let { isOpenM, userInfo } = useSelector(state => state.adminSlice)
    const [gender, setGender] = useState(null);

    const handleAddUser = () => {
        dispatch(showAddModal());
    };
    const handleCancel = () => {
        dispatch(handleCloseModal());
        window.location.reload();
    };

    const onFinish = (values) => {
        if (userInfo) {
            adminServ.updateUser(values.id, values)
                .then((res) => {
                    message.success('Cập nhật người dùng thành công');
                    setTimeout(() => {
                        window.location.reload();
                    }, 1000);
                    window.location.reload();
                })
                .catch((err) => {
                    message.error("Đã có lỗi xảy ra");
                });
        }
        else {
            adminServ.addUser(values)
                .then((res) => {
                    message.success('Thêm người dùng thành công');
                    setTimeout(() => {
                        window.location.reload();
                    }, 1000);
                })
                .catch((err) => {
                    message.error("Đã có lỗi xảy ra");
                });
        }

    };
    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <div id='addEditUserModal' className='addEditModal'>
            <Button onClick={handleAddUser} className='add'>
                Thêm quản trị viên
            </Button>
            <Modal title={userInfo ? "Chỉnh sửa thông tin người dùng" : "Thêm người dùng mới"} open={isOpenM} footer={false} onCancel={handleCancel}>
                <Form
                    name="basic"
                    labelCol={{
                        span: 8,
                    }}
                    wrapperCol={{
                        span: 16,
                    }}
                    style={{
                        maxWidth: 600,
                    }}
                    initialValues={{
                        id: userInfo ? userInfo.id : 0,
                        name: userInfo ? userInfo.name : null,
                        email: userInfo ? userInfo.email : null,
                        phone: userInfo ? userInfo.phone : null,
                        birthday: userInfo ? userInfo.birthday : null,
                        gender: userInfo ? userInfo.gender : null,
                        role: userInfo ? userInfo.role : 'ADMIN',
                        password: userInfo ? userInfo.password : null,
                    }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    autoComplete="off"
                >
                    <Form.Item
                        label="ID"
                        name="id"
                        rules={[
                            {
                                required: false,
                                message: 'Mục không được để trống!',
                            },
                        ]}
                        className='userID'
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="Username"
                        name="name"
                        rules={[
                            {
                                required: true,
                                message: 'Mục không được để trống!',
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="Email"
                        name="email"
                        rules={[
                            {
                                required: true,
                                message: 'Mục không được để trống!',
                            },
                            {
                                type: "email",
                                message: 'Email không hợp lệ!',
                            },

                        ]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="Phone number"
                        name="phone"
                        rules={[
                            {
                                required: true,
                                message: 'Mục không được để trống!',
                            },
                            {
                                len: 10,
                                message: 'Số điện thoại gồm 10 ký tự số'
                            }

                        ]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="Birthday"
                        name="birthday"
                        rules={[
                            {
                                required: false,
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="Gender"
                        name="gender"
                        rules={[
                            {
                                required: true,
                            },
                        ]}
                    >
                        <Radio.Group value={gender}>
                            <Radio value={true}>Nam</Radio>
                            <Radio value={false}>Nữ</Radio>
                        </Radio.Group>
                    </Form.Item>

                    <Form.Item
                        label="Password"
                        name="password"
                        rules={[
                            {
                                required: true,
                                message: 'Mục không được để trống!'
                            },
                            {
                                min: 8,
                                message: 'Mật khẩu ít nhất 4 ký tự'
                            },
                        ]}
                    >
                        <Input.Password />
                    </Form.Item>

                    <Form.Item
                        label="Role"
                        name="role"
                        rules={[
                            {
                                required: false,
                                message: 'Mục không được để trống!',
                            },
                        ]}
                        className='userRole'
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        wrapperCol={{
                            offset: 8,
                            span: 16,
                        }}
                    >
                        <Button htmlType="submit" className='addEditBtn mr-3'>
                            {userInfo ? "Cập nhật" : "Thêm"}
                        </Button>
                        <Button onClick={handleCancel}>Hủy</Button>
                    </Form.Item>
                </Form>
            </Modal>
        </div>
    )
}
