import React from 'react'
import { Desktop, LargeDesktop, Mobile, SmallMobile, Tablet } from '../../Layouts/Responsive'
import FooterMobile from './FooterMobile'
import FooterTablet from './FooterTablet'
import FooterDesktop from './FooterDesktop'
import FooterSmallMobile from './FooterSmallMobile'
import '../../css/footer.css'

export default function Footer() {
    return (
        <>
            <SmallMobile>
                <FooterSmallMobile />
            </SmallMobile>
            <Mobile>
                <FooterMobile />
            </Mobile>
            <Tablet>
                <FooterTablet />
            </Tablet>
            <Desktop>
                <FooterDesktop />
            </Desktop>
            <LargeDesktop>
                <FooterDesktop />
            </LargeDesktop>
        </>
    )
}
